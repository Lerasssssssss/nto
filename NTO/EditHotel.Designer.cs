﻿namespace NTO
{
    partial class EditHotel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tbEdNameHotel = new TextBox();
            cbEdRegion = new ComboBox();
            btnEdHotel = new Button();
            SuspendLayout();
            // 
            // tbEdNameHotel
            // 
            tbEdNameHotel.Location = new Point(36, 37);
            tbEdNameHotel.Name = "tbEdNameHotel";
            tbEdNameHotel.Size = new Size(228, 23);
            tbEdNameHotel.TabIndex = 0;
            tbEdNameHotel.Text = "Название отеля";
            // 
            // cbEdRegion
            // 
            cbEdRegion.FormattingEnabled = true;
            cbEdRegion.Location = new Point(35, 92);
            cbEdRegion.Name = "cbEdRegion";
            cbEdRegion.Size = new Size(229, 23);
            cbEdRegion.TabIndex = 1;
            cbEdRegion.Text = "Регион";
            // 
            // btnEdHotel
            // 
            btnEdHotel.Location = new Point(555, 387);
            btnEdHotel.Name = "btnEdHotel";
            btnEdHotel.Size = new Size(172, 36);
            btnEdHotel.TabIndex = 2;
            btnEdHotel.Text = "Сохранить изменения";
            btnEdHotel.UseVisualStyleBackColor = true;
            // 
            // EditHotel
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(btnEdHotel);
            Controls.Add(cbEdRegion);
            Controls.Add(tbEdNameHotel);
            Name = "EditHotel";
            Text = "EditHotel";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private TextBox tbEdNameHotel;
        private ComboBox cbEdRegion;
        private Button btnEdHotel;
    }
}