﻿namespace NTO
{
    partial class EditRoom
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tbEdRoom = new TextBox();
            cbEdCategory = new ComboBox();
            tbEdPeople = new TextBox();
            cbEdStatus = new ComboBox();
            btnEdRoom = new Button();
            SuspendLayout();
            // 
            // tbEdRoom
            // 
            tbEdRoom.Location = new Point(35, 32);
            tbEdRoom.Name = "tbEdRoom";
            tbEdRoom.Size = new Size(201, 23);
            tbEdRoom.TabIndex = 0;
            tbEdRoom.Text = "Номер";
            // 
            // cbEdCategory
            // 
            cbEdCategory.FormattingEnabled = true;
            cbEdCategory.Location = new Point(34, 78);
            cbEdCategory.Name = "cbEdCategory";
            cbEdCategory.Size = new Size(202, 23);
            cbEdCategory.TabIndex = 1;
            cbEdCategory.Text = "Категория";
            // 
            // tbEdPeople
            // 
            tbEdPeople.Location = new Point(403, 32);
            tbEdPeople.Name = "tbEdPeople";
            tbEdPeople.Size = new Size(176, 23);
            tbEdPeople.TabIndex = 2;
            tbEdPeople.Text = "Посетители";
            // 
            // cbEdStatus
            // 
            cbEdStatus.FormattingEnabled = true;
            cbEdStatus.Location = new Point(402, 79);
            cbEdStatus.Name = "cbEdStatus";
            cbEdStatus.Size = new Size(177, 23);
            cbEdStatus.TabIndex = 3;
            cbEdStatus.Text = "Статус";
            // 
            // btnEdRoom
            // 
            btnEdRoom.Location = new Point(566, 374);
            btnEdRoom.Name = "btnEdRoom";
            btnEdRoom.Size = new Size(188, 40);
            btnEdRoom.TabIndex = 4;
            btnEdRoom.Text = "Сохранить изменения";
            btnEdRoom.UseVisualStyleBackColor = true;
            // 
            // EditRoom
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(btnEdRoom);
            Controls.Add(cbEdStatus);
            Controls.Add(tbEdPeople);
            Controls.Add(cbEdCategory);
            Controls.Add(tbEdRoom);
            Name = "EditRoom";
            Text = "EditRoom";
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private TextBox tbEdRoom;
        private ComboBox cbEdCategory;
        private TextBox tbEdPeople;
        private ComboBox cbEdStatus;
        private Button btnEdRoom;
    }
}