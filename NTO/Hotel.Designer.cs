﻿namespace NTO
{
    partial class Hotel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            dgvHotel = new DataGridView();
            Name = new DataGridViewTextBoxColumn();
            Region = new DataGridViewTextBoxColumn();
            btnRoom = new Button();
            label1 = new Label();
            ((System.ComponentModel.ISupportInitialize)dgvHotel).BeginInit();
            SuspendLayout();
            // 
            // dgvHotel
            // 
            dgvHotel.AllowUserToAddRows = false;
            dgvHotel.AllowUserToDeleteRows = false;
            dgvHotel.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dgvHotel.Columns.AddRange(new DataGridViewColumn[] { Name, Region });
            dgvHotel.Location = new Point(32, 39);
            dgvHotel.Name = "dgvHotel";
            dgvHotel.ReadOnly = true;
            dgvHotel.RowTemplate.Height = 25;
            dgvHotel.Size = new Size(734, 320);
            dgvHotel.TabIndex = 0;
            // 
            // Name
            // 
            Name.DataPropertyName = "Name";
            Name.HeaderText = "Название отеля";
            Name.Name = "Name";
            Name.ReadOnly = true;
            Name.Width = 400;
            // 
            // Region
            // 
            Region.DataPropertyName = "NameRegion";
            Region.HeaderText = "Регион";
            Region.Name = "Region";
            Region.ReadOnly = true;
            // 
            // btnRoom
            // 
            btnRoom.Location = new Point(627, 381);
            btnRoom.Name = "btnRoom";
            btnRoom.Size = new Size(139, 36);
            btnRoom.TabIndex = 1;
            btnRoom.Text = "Номера отеля";
            btnRoom.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(35, 12);
            label1.Name = "label1";
            label1.Size = new Size(38, 15);
            label1.TabIndex = 2;
            label1.Text = "label1";
            // 
            // Hotel
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(label1);
            Controls.Add(btnRoom);
            Controls.Add(dgvHotel);
            Text = "Hotel";
            Load += Hotel_Load;
            ((System.ComponentModel.ISupportInitialize)dgvHotel).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private DataGridView dgvHotel;
        private Button btnRoom;
        private Label label1;
        private DataGridViewTextBoxColumn Name;
        private DataGridViewTextBoxColumn Region;
    }
}