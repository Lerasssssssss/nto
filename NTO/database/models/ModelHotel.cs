﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NTO.database.models
{
    internal class ModelHotel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public Guid RegionId { get; set; }

        public ModelRegion Region { get; set; }

        public string NameRegion { get { return Region.Name; } }
    }
}
