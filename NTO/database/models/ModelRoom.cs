﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NTO.database.models
{
    internal class ModelRoom
    {
        public Guid Id { get; set; }
        public string Number { get; set; }
        public Guid HotelId { get; set; }

        [NotMapped]
        public Hotel Hotel { get; set; }

        public int CountHumans { get; set; }

        [NotMapped]
        public Guid CategoryId { get; set; }

        public ModelCategory Category { get; set; }

        public Guid StatusId { get; set; }

        public ModelStatus Status { get; set; }
    }
}
