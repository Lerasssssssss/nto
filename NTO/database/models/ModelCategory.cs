﻿using System.ComponentModel.DataAnnotations.Schema;

namespace NTO.database.models
{
    internal class ModelCategory
    {
        public Guid Id { get; set; }
    
        public Guid HotelId { get; set; }

        [NotMapped]
        public ModelHotel Hotel { get; set; }

        public string Name { get; set; }

        public int MaxCountHumans { get; set; }

        public int Price { get; set; }
    }
}
