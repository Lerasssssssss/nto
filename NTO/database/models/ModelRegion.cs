﻿
namespace NTO.database.models
{
    internal class ModelRegion
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
