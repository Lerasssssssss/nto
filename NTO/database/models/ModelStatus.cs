﻿namespace NTO.database.models
{
    internal class ModelStatus
    {
        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
