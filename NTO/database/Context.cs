﻿using Microsoft.EntityFrameworkCore;
using NTO.database.models;

namespace NTO.database
{
    internal class Context: DbContext
    {
        public static string PathDirectoryDB = "C:\\NTO\\";
        public static string NameFileDB = "finalnto.db";
        public static string ConnectionString = $"Data Source={PathDirectoryDB}\\{NameFileDB}";

        public Context() => Database.EnsureCreated();

        public DbSet<ModelHotel> Hotels => Set<ModelHotel>();
        public DbSet<ModelRegion> Regions => Set<ModelRegion>();
        public DbSet<ModelCategory> Categories => Set<ModelCategory>();
        public DbSet<ModelRoom> Rooms => Set<ModelRoom>();

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            Directory.CreateDirectory(PathDirectoryDB);
            optionsBuilder.UseSqlite(ConnectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ModelRegion>().HasData(
                new ModelRegion { Id = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f5"), Name = "Москва" },
                new ModelRegion { Id = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f9"), Name = "Нижний Новгород" },
                new ModelRegion { Id = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f3"), Name = "Санкт-Петербург" },
                new ModelRegion { Id = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f0"), Name = "Киров" },
                new ModelRegion { Id = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f4"), Name = "Екатеринбург" },
                new ModelRegion { Id = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f1"), Name = "Ижевск" },
                new ModelRegion { Id = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f2"), Name = "Пенза" },
                new ModelRegion { Id = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f6"), Name = "Сыктывкар" },
                new ModelRegion { Id = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f7"), Name = "Калининград" },
                new ModelRegion { Id = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f8"), Name = "Павловский Посад" }
            ) ;

            modelBuilder.Entity<ModelHotel>().HasData(
                new ModelHotel { Id = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f287"), Name = "Весенние ласточки", RegionId = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f5")},
                new ModelHotel { Id = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f281"), Name = "Капель", RegionId = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f9") },
                new ModelHotel { Id = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f282"), Name = "Здоровый отдых", RegionId = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f3") },
                new ModelHotel { Id = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f283"), Name = "Переправа", RegionId = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f0") },
                new ModelHotel { Id = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f284"), Name = "Лесной Бор", RegionId = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f4") },
                new ModelHotel { Id = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f285"), Name = "Неограниченные возможности", RegionId = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f1") },
                new ModelHotel { Id = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f286"), Name = "Чистый лист", RegionId = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f2") },
                new ModelHotel { Id = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f288"), Name = "Солнечный день", RegionId = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f6") },
                new ModelHotel { Id = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f289"), Name = "Шоколад", RegionId = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f7") },
                new ModelHotel { Id = Guid.Parse("0596eb96-6f62-4aad-8aa3-1a8ce754f280"), Name = "Яблочный спас", RegionId = Guid.Parse("cb7eba6d-6917-467a-8805-06e589fb73f8") }
            );
        }

        public List<ModelHotel> GetHotels() 
        { 
            return Hotels.Include(t => t.Region).ToList();
        }

        public List<ModelRegion> GetRegions()
        {
            return Regions.ToList();
        }

        public List<ModelRoom> GetRoomsHotel(Guid hotelId) 
        {
            return Rooms
                .Where(t => t.HotelId == hotelId)
                .Include(t => t.Category)
                .Include(t => t.Stastus)
                .ToList();
        }

        public List<ModelCategory> GetCategories()
        {
            return Categories.ToList();
        }
    }
}
