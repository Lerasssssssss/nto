﻿using NTO.database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NTO
{
    public partial class Hotel : Form
    {
        public Hotel()
        {
            InitializeComponent();
        }

        private void Hotel_Load(object sender, EventArgs e)
        {
            dgvHotel.AutoGenerateColumns = false;
            using (Context context = new())
            { 
                dgvHotel.DataSource = context.GetHotels();
            }
        }
    }
}
