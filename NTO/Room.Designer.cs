﻿namespace NTO
{
    partial class Room
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            dataGridView1 = new DataGridView();
            Rooms = new DataGridViewTextBoxColumn();
            Category = new DataGridViewTextBoxColumn();
            CountHumans = new DataGridViewTextBoxColumn();
            Status = new DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)dataGridView1).BeginInit();
            SuspendLayout();
            // 
            // dataGridView1
            // 
            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.AllowUserToDeleteRows = false;
            dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridView1.Columns.AddRange(new DataGridViewColumn[] { Rooms, Category, CountHumans, Status });
            dataGridView1.Location = new Point(28, 40);
            dataGridView1.Name = "dataGridView1";
            dataGridView1.ReadOnly = true;
            dataGridView1.RowTemplate.Height = 25;
            dataGridView1.Size = new Size(738, 286);
            dataGridView1.TabIndex = 0;
            // 
            // Rooms
            // 
            Rooms.HeaderText = "Номер";
            Rooms.Name = "Rooms";
            Rooms.ReadOnly = true;
            // 
            // Category
            // 
            Category.HeaderText = "Категория";
            Category.Name = "Category";
            Category.ReadOnly = true;
            // 
            // CountHumans
            // 
            CountHumans.HeaderText = "Количество мест";
            CountHumans.Name = "CountHumans";
            CountHumans.ReadOnly = true;
            // 
            // Status
            // 
            Status.HeaderText = "Статус";
            Status.Name = "Status";
            Status.ReadOnly = true;
            // 
            // Room
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(800, 450);
            Controls.Add(dataGridView1);
            Name = "Room";
            Text = "Room";
            ((System.ComponentModel.ISupportInitialize)dataGridView1).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private DataGridView dataGridView1;
        private DataGridViewTextBoxColumn Rooms;
        private DataGridViewTextBoxColumn Category;
        private DataGridViewTextBoxColumn CountHumans;
        private DataGridViewTextBoxColumn Status;
    }
}